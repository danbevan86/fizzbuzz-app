<?php

    function checkPrime($num) {
        if ($num == 1) { // 1 is not a prime number
            return 0; 
        }
        for ($i = 2; $i <= $num/2; $i++){ 
            if ($num % $i == 0) {
                return 0; 
            }
        }
        return 1; 
    }

    function outputToLog($data) {
        file_put_contents('fizzbuzz.log', $data, FILE_APPEND);
    }

    for ($i = 1; $i <= 500; $i++) {
        if (checkPrime($i)) {
            outputToLog($i . " FizzBuzz++"."\n");
        } else if ( $i%3 == 0 && $i%5 == 0 ) { // if number is divisble by 5 and 3
            outputToLog($i . " FizzBuzz"."\n"); // output number + "FizzBuzz"
        } else if ( $i%3 == 0 ) { // if number is divisible by 3
            outputToLog($i. " Fizz"."\n"); // output number + "Fizz"
        } else if ( $i%5 == 0 ) { // if number is divisible by 5
            outputToLog($i. " Buzz"."\n");// output number + "Buzz"
        } else {
            outputToLog($i."\n"); // output number only
        }
    }
?>